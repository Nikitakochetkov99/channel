package com.kochetkov.channel.dto.post;

import com.kochetkov.channel.entity.ChannelEntity;
import lombok.Data;

import java.util.List;

@Data
public class PostCreateDto {

    private String title;

    private String content;

    private Integer channelId;
}
