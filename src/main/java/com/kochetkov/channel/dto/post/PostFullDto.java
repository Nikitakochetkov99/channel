package com.kochetkov.channel.dto.post;

import com.kochetkov.channel.dto.channel.ChannelFullDto;
import lombok.Data;
import java.util.List;

@Data
public class PostFullDto {

    private Integer id;

    private String title;

    private String content;

    private Integer channelId;
}
