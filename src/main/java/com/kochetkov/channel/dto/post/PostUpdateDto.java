package com.kochetkov.channel.dto.post;

import lombok.Data;

@Data
public class PostUpdateDto {

    private Integer id;

    private String title;

    private String content;
}
