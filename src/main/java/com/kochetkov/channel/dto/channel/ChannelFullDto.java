package com.kochetkov.channel.dto.channel;

import com.kochetkov.channel.dto.post.PostFullDto;
import com.kochetkov.channel.entity.ChannelEntityType;
import lombok.Data;

import java.util.List;

@Data
public class ChannelFullDto {

    private Integer id;

    private String name;

    private ChannelEntityType type;

    private List<PostFullDto> posts;
}
