package com.kochetkov.channel.dto.channel;

import com.kochetkov.channel.entity.ChannelEntityType;
import lombok.Data;

@Data
public class ChannelUpdateDto {

    private Integer id;

    private String name;

    private ChannelEntityType type;
}
