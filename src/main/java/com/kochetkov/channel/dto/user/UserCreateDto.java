package com.kochetkov.channel.dto.user;

import lombok.Data;

@Data
public class UserCreateDto {

    private String login;

    private String password;

    private String imageUrl;

    private String email;
}
