package com.kochetkov.channel.dto.user;

import lombok.Data;

@Data
public class UserUpdateDto {

    private Integer id; // id обновляемого уровня
    private String email;
    private String imageUrl;
}
