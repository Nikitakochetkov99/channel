package com.kochetkov.channel.service;

import com.kochetkov.channel.dto.channel.ChannelCreateDto;
import com.kochetkov.channel.dto.channel.ChannelFullDto;
import com.kochetkov.channel.dto.channel.ChannelUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ChannelService {

    ChannelFullDto findById(Integer id);

    List<ChannelFullDto> findAll();

    ChannelFullDto create(ChannelCreateDto createDto);

    ChannelFullDto update(ChannelUpdateDto updateDto);

    void deleteById(Integer id);
}
