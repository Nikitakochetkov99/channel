package com.kochetkov.channel.service;

import com.kochetkov.channel.dto.post.PostCreateDto;
import com.kochetkov.channel.dto.post.PostFullDto;
import com.kochetkov.channel.dto.post.PostUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {

    PostFullDto findById(Integer id);

    List<PostFullDto> findAll();

    PostFullDto create(PostCreateDto createDto);

    PostFullDto update(PostUpdateDto updateDto);

    void deleteById(Integer id);
}
