package com.kochetkov.channel.service.impl;

import com.kochetkov.channel.dto.channel.ChannelCreateDto;
import com.kochetkov.channel.dto.channel.ChannelFullDto;
import com.kochetkov.channel.dto.channel.ChannelUpdateDto;
import com.kochetkov.channel.entity.ChannelEntity;
import com.kochetkov.channel.exeption.ChannelCredentialslsAreTakenException;
import com.kochetkov.channel.exeption.EntityIsNotFoundExeption;
import com.kochetkov.channel.repository.ChannelRepository;
import com.kochetkov.channel.service.ChannelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.kochetkov.channel.mapper.ChannelMapper.CHANNEL_MAPPER;

@Slf4j
@Service
public class ChannelServiceImpl implements ChannelService {

    @Autowired
    private ChannelRepository channelRepository;

    @Override
    @Transactional(readOnly = true)
    public ChannelFullDto findById(Integer id) {
        ChannelEntity findByChannelId = channelRepository.findOneById(id);

        if (findByChannelId == null) { //
            throw new EntityIsNotFoundExeption("Channel was not found by id: " + id);
        }

        log.info("ChannelServiceImpl -> found channel: {} by id: {}", findByChannelId, id);
        return CHANNEL_MAPPER.mapToFullDto(findByChannelId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ChannelFullDto> findAll() {
        List<ChannelEntity> findAllChannels = channelRepository.findAll();
        List<ChannelFullDto> channelFullDtos = new ArrayList<>();

        findAllChannels.forEach(channelEntity
                -> channelFullDtos.add(CHANNEL_MAPPER.mapToFullDto(channelEntity)));

        log.info("ChannelServiceImpl -> found {} channels", channelFullDtos.size());
        return channelFullDtos;
    }

    @Override
    @Transactional
    public ChannelFullDto create(ChannelCreateDto createDto) {
        ChannelEntity toSave = CHANNEL_MAPPER.mapToEntity(createDto);

        checkIfTypeOrNameIsTaken(toSave);

        ChannelEntity savedEntity = channelRepository.save(toSave);

        log.info("ChannelServiceImpl -> channel {} successfully saved", savedEntity);
        return CHANNEL_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public ChannelFullDto update(ChannelUpdateDto updateDto) {
        ChannelEntity entityToUpDate = channelRepository.findOneById(updateDto.getId());

        if (entityToUpDate == null) {
            throw new EntityIsNotFoundExeption("Channel was not found by id: " + updateDto.getId());
        }

        entityToUpDate.setType(updateDto.getType());
        entityToUpDate.setName(updateDto.getName());

        ChannelEntity updatedEntity = channelRepository.save(entityToUpDate);

        log.info("ChannelServiceImpl -> channel {} was successfully updated", updatedEntity);
        return CHANNEL_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if (!channelRepository.existsById(id)) {
            throw new EntityIsNotFoundExeption("Channel was not found by id: " + id);
        }
        channelRepository.deleteById(id);

    }

    private void checkIfTypeOrNameIsTaken(ChannelEntity entity) {
        List<ChannelEntity> foundChannels = channelRepository.findByTypeAndName(entity.getType(), entity.getName());

        foundChannels.stream()
                .filter(channelEntity -> !channelEntity.getId().equals(entity.getId()))
                .forEach(channelEntity -> {
                    throw new ChannelCredentialslsAreTakenException(
                            "Type:" + entity.getType() + "Name:" + entity.getName());
                });
    }
}
