package com.kochetkov.channel.service.impl;

import com.kochetkov.channel.dto.post.PostCreateDto;
import com.kochetkov.channel.dto.post.PostFullDto;
import com.kochetkov.channel.dto.post.PostUpdateDto;
import com.kochetkov.channel.entity.PostEntity;
import com.kochetkov.channel.exeption.EntityIsNotFoundExeption;
import com.kochetkov.channel.exeption.PostCredentialslsAreTakenException;
import com.kochetkov.channel.repository.PostRepository;
import com.kochetkov.channel.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.kochetkov.channel.mapper.PostMapper.POST_MAPPER;

@Slf4j
@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Override
    @Transactional(readOnly = true)
    public PostFullDto findById(Integer id) {
        PostEntity foundByPostId = postRepository.findOneById(id);

        if (foundByPostId == null) { //
            throw new EntityIsNotFoundExeption("Post was not found by id: " + id);
        }

        log.info("PostServiceImpl -> found post: {} by id: {}", foundByPostId, id);
        return POST_MAPPER.mapToFullDto(foundByPostId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PostFullDto> findAll() {
        List<PostEntity> foundAllPosts = postRepository.findAll();
        List<PostFullDto> postFullDtos = new ArrayList<>();

        foundAllPosts.forEach(postEntity
                -> postFullDtos.add(POST_MAPPER.mapToFullDto(postEntity)));

        log.info("PostServiceImpl -> found {} posts", postFullDtos.size());
        return postFullDtos;
    }

    @Override
    @Transactional
    public PostFullDto create(PostCreateDto createDto) {
        PostEntity toSave = POST_MAPPER.mapToEntity(createDto);

        checkIfTitleOrContentIsTaken(toSave);

        PostEntity savedEntity = postRepository.save(toSave);

        log.info("PostServiceImpl -> post {} successfully saved", savedEntity);
        return POST_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public PostFullDto update(PostUpdateDto updateDto) {
        PostEntity entityToUpdate = postRepository.findOneById(updateDto.getId());

        if (entityToUpdate == null) {
            throw new EntityIsNotFoundExeption("Post was not found by id: " + updateDto.getId());
        }

        entityToUpdate.setTitle(updateDto.getTitle());
        entityToUpdate.setContent(updateDto.getContent());

        PostEntity updatedEntity = postRepository.save(entityToUpdate);

        log.info("PostServiceImpl -> post {} was successfully updated", updatedEntity);
        return POST_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if (!postRepository.existsById(id)) {
            throw new EntityIsNotFoundExeption("Post was not found by id: " + id);
        }
        postRepository.deleteById(id);

    }

    private void checkIfTitleOrContentIsTaken(PostEntity entity) {
        List<PostEntity> foundPosts = postRepository.findByTitleAndContent(entity.getTitle(), entity.getContent());

        foundPosts.stream()
                .filter(postEntity ->!postEntity.getId().equals(entity.getId()))
                .forEach(postEntity -> {
                    throw new PostCredentialslsAreTakenException(
                            "Title:" + entity.getTitle() + "Conyent:" + entity.getContent());
                });
    }
}
