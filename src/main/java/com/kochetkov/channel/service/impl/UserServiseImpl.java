package com.kochetkov.channel.service.impl;

import com.kochetkov.channel.dto.user.UserCreateDto;
import com.kochetkov.channel.dto.user.UserFullDto;
import com.kochetkov.channel.dto.user.UserUpdateDto;
import com.kochetkov.channel.entity.UserEntity;
import static com.kochetkov.channel.mapper.UserMapper.USER_MAPPER;
import com.kochetkov.channel.exeption.EntityIsNotFoundExeption;
import com.kochetkov.channel.exeption.UserCredentialslsAreTakenException;
import com.kochetkov.channel.repository.UserRepository;
import com.kochetkov.channel.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class UserServiseImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserFullDto findById(Integer id) {
        UserEntity foundUserById = userRepository.findOneById(id);
        if (foundUserById == null) { //
            throw new EntityIsNotFoundExeption("User was not found by id: " + id);
        }
        log.info("UserServiceImpl -> found user: {} by id: {}", foundUserById, id);
        return USER_MAPPER.mapToFullDto(foundUserById);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserFullDto> findAll() {
        List<UserEntity> foundAllUsers = userRepository.findAll();

        List<UserFullDto> users = new ArrayList<>();

        foundAllUsers.forEach(userEntity
                -> users.add(USER_MAPPER.mapToFullDto(userEntity)));

        log.info("UserServiceImpl -> found {} users", users.size());
        return users;
    }

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto createDto) {
        UserEntity toSave = USER_MAPPER.mapToEntity(createDto);

        checkIfLoginOrEmailIsTaken(toSave);

        UserEntity savedEntity = userRepository.save(toSave);

        log.info("UserServiceImpl -> user {} successfully saved", savedEntity);
        return USER_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto updateDto) {
        UserEntity entityToUpdate = userRepository.findOneById(updateDto.getId());
        if (entityToUpdate == null) {
            throw new EntityIsNotFoundExeption("User was not found by id: " + updateDto.getId());
        }

        entityToUpdate.setEmail(updateDto.getEmail());
        entityToUpdate.setImageUrl(updateDto.getImageUrl());

        UserEntity updatedEntity = userRepository.save(entityToUpdate);

        log.info("UserServiceImpl -> user {} was successfully updated", updatedEntity);
        return USER_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if (!userRepository.existsById(id)) {
            throw new EntityIsNotFoundExeption("User was not found by id: " + id);
        }
        userRepository.deleteById(id);
    }

    private void checkIfLoginOrEmailIsTaken(UserEntity entity) {
        List<UserEntity> foundUsers = userRepository.findByLoginOrEmail(entity.getLogin(), entity.getEmail());

        foundUsers.stream()
                .filter(userEntity ->!userEntity.getId().equals(entity.getId()))
                .forEach(userEntity -> {
                    throw new UserCredentialslsAreTakenException(
                            "Email:" + entity.getEmail() + "Login:" + entity.getLogin());
                });
    }
}
