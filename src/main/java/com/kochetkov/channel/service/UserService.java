package com.kochetkov.channel.service;

import com.kochetkov.channel.dto.user.UserCreateDto;
import com.kochetkov.channel.dto.user.UserFullDto;
import com.kochetkov.channel.dto.user.UserUpdateDto;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface UserService {

    UserFullDto findById(Integer id);

    List<UserFullDto> findAll();

    UserFullDto create(UserCreateDto createDto);

    UserFullDto update(UserUpdateDto updateDto);

    void deleteById(Integer id);
}
