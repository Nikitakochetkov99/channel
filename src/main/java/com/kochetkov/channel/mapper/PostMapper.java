package com.kochetkov.channel.mapper;

import com.kochetkov.channel.dto.post.PostCreateDto;
import com.kochetkov.channel.dto.post.PostFullDto;
import com.kochetkov.channel.entity.PostEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PostMapper {


    PostMapper POST_MAPPER = Mappers.getMapper(PostMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "channel", ignore = true)
    PostEntity mapToEntity(PostCreateDto createDto);

    PostFullDto mapToFullDto(PostEntity postEntity);

    @Mapping(target = "channelId", source = "channel.id")
    PostFullDto toDto(PostEntity entity);
}
