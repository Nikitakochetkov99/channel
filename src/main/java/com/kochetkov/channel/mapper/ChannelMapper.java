package com.kochetkov.channel.mapper;

import com.kochetkov.channel.dto.channel.ChannelCreateDto;
import com.kochetkov.channel.dto.channel.ChannelFullDto;
import com.kochetkov.channel.entity.ChannelEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ChannelMapper {

    ChannelMapper CHANNEL_MAPPER = Mappers.getMapper(ChannelMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "posts", ignore = true)
    ChannelEntity mapToEntity(ChannelCreateDto createDto);

    ChannelFullDto mapToFullDto(ChannelEntity channelEntity);

    @Mapping(target = "postId", source = "post.id")
    ChannelFullDto toDto(ChannelEntity channelEntity);
}
