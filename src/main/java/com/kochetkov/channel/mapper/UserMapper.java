package com.kochetkov.channel.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import com.kochetkov.channel.entity.UserEntity;
import com.kochetkov.channel.dto.user.UserFullDto;
import com.kochetkov.channel.dto.user.UserCreateDto;


@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "profileImageUrl", source = "imageUrl")
    @Mapping(target = "phoneNumber", source = "id")
    UserFullDto mapToFullDto(UserEntity entity);

    @Mapping(target = "id", ignore = true)
    UserEntity mapToEntity(UserCreateDto dto);

    default String map(Integer number) {
        return String.valueOf(number * 999);
    }
}
