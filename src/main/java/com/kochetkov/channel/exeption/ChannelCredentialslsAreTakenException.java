package com.kochetkov.channel.exeption;

public class ChannelCredentialslsAreTakenException extends RuntimeException {

    public ChannelCredentialslsAreTakenException(String message) {
        super(message);
    }
}
