package com.kochetkov.channel.exeption;

public class EntityIsNotFoundExeption extends RuntimeException {

    public EntityIsNotFoundExeption(String massage) {
        super(massage);
    }
}
