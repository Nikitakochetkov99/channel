package com.kochetkov.channel.exeption;

public class UserCredentialslsAreTakenException extends RuntimeException {

    public UserCredentialslsAreTakenException(String message) {
        super(message);
    }
}
