package com.kochetkov.channel.exeption;

public class PostCredentialslsAreTakenException extends RuntimeException{

    public PostCredentialslsAreTakenException(String message){
        super(message);
    }
}
