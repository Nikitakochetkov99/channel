package com.kochetkov.channel.controller;

import com.kochetkov.channel.dto.post.PostCreateDto;
import com.kochetkov.channel.dto.post.PostFullDto;
import com.kochetkov.channel.dto.post.PostUpdateDto;
import com.kochetkov.channel.service.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Controller dedicated to manage posts")
@RestController
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping("/posts/{id}")
    @ApiOperation(value = "Find one post by id", notes = "Existing id must be specified")
    public PostFullDto getById(@PathVariable Integer id) {
        return postService.findById(id);
    }

    @GetMapping("/posts")
    public List<PostFullDto> findAll() {
        return postService.findAll();
    }

    @PostMapping("/posts")
    public PostFullDto create(@RequestBody PostCreateDto createDto) {
        return postService.create(createDto);
    }

    @PutMapping("/posts")
    public PostFullDto update(@RequestBody PostUpdateDto updateDto) {
        return postService.update(updateDto);
    }

    @DeleteMapping("/posts/{id}")
    public void delete(@PathVariable Integer id) {
        postService.deleteById(id);
    }

    @GetMapping("/test-create")
    public void test() {
        for (int i = 0; i < 10; i++) {
            PostCreateDto createDto = new PostCreateDto();
            createDto.setContent("content" + Math.random());
            createDto.setTitle("title" + Math.random());
            createDto.setChannelId(1);

            postService.create(createDto);
        }
    }
}
