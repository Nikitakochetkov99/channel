package com.kochetkov.channel.controller;

import com.kochetkov.channel.dto.user.UserCreateDto;
import com.kochetkov.channel.dto.user.UserFullDto;
import com.kochetkov.channel.dto.user.UserUpdateDto;
import com.kochetkov.channel.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Controller dedicated to manage users")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users/{id}")
    @ApiOperation(value = "Find one user by id", notes = "Existing id must be specified")
    public UserFullDto getById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    public List<UserFullDto> findAll() {
        return userService.findAll();
    }

    @PostMapping("/users")
    public UserFullDto create(@RequestBody UserCreateDto createDto) {
        return userService.create(createDto);
    }

    @PutMapping("/users")
    public UserFullDto update(@RequestBody UserUpdateDto updateDto) {
        return userService.update(updateDto);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer id) {
        userService.deleteById(id);
    }

    @GetMapping("/test-create")
    public void test() {
        for (int i = 0; i < 10; i++) {
            UserCreateDto createDto = new UserCreateDto();
            createDto.setEmail("email" + Math.random());
            createDto.setLogin("login" + Math.random());
            createDto.setPassword("password" + Math.random());
            createDto.setImageUrl("image" + Math.random());

            userService.create(createDto);
        }
    }
}
