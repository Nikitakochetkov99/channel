package com.kochetkov.channel.controller;

import com.kochetkov.channel.dto.channel.ChannelCreateDto;
import com.kochetkov.channel.dto.channel.ChannelFullDto;
import com.kochetkov.channel.dto.channel.ChannelUpdateDto;
import com.kochetkov.channel.entity.ChannelEntityType;
import com.kochetkov.channel.service.ChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Controller dedicated to manage channels")
@RestController
public class ChannelController {

    @Autowired
    private ChannelService channelService;

    @GetMapping("/channels/{id}")
    @ApiOperation(value = "Find one channel by id", notes = "Existing id must be specified")
    public ChannelFullDto getById(@PathVariable Integer id) {
        return channelService.findById(id);
    }

    @GetMapping("/channels")
    public List<ChannelFullDto> findAll() {
        return channelService.findAll();
    }

    @PostMapping("/channels")
    public ChannelFullDto create(@RequestBody ChannelCreateDto createDto) {
        return channelService.create(createDto);
    }

    @PutMapping("/channels")
    public ChannelFullDto update(@RequestBody ChannelUpdateDto updateDto) {
        return channelService.update(updateDto);
    }

    @DeleteMapping("/channels/{id}")
    public void delete(@PathVariable Integer id) {
        channelService.deleteById(id);
    }

    @GetMapping("/test-create")
    public void test() {
        for (int i = 0; i < 10; i++) {
            ChannelCreateDto createDto = new ChannelCreateDto();
            createDto.setName("name" + Math.random());
            createDto.setType(ChannelEntityType.TOURISM);

            channelService.create(createDto);
        }
    }
}
