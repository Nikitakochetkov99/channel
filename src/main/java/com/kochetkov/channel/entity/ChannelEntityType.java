package com.kochetkov.channel.entity;

public enum ChannelEntityType {

    PROGRAMMING,
    EDUCATION,
    TOURISM
}
