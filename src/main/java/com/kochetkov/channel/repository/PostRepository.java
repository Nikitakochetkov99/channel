package com.kochetkov.channel.repository;

import com.kochetkov.channel.entity.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<PostEntity, Integer> {

    PostEntity findByTitle(String title);

    PostEntity findOneById(Integer id);

    List<PostEntity> findByTitleAndContent(String title, String content);
}
