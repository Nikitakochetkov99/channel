package com.kochetkov.channel.repository;

import com.kochetkov.channel.entity.ChannelEntity;
import com.kochetkov.channel.entity.ChannelEntityType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ChannelRepository extends JpaRepository<ChannelEntity, Integer>{

    ChannelEntity findByType(ChannelEntityType type);

    ChannelEntity findOneById(Integer id);

    List<ChannelEntity> findByTypeAndName(ChannelEntityType type, String name);
}
