package com.kochetkov.channel;

import com.kochetkov.channel.entity.ChannelEntity;
import com.kochetkov.channel.entity.ChannelEntityType;
import com.kochetkov.channel.entity.PostEntity;

import java.util.Arrays;
import java.util.List;

public class EntityGenerationUtils {

    public static ChannelEntity generateChannelEntity() {
        ChannelEntity entity = new ChannelEntity();
        entity.setName("Name#" + Math.random() * 100);
        entity.setType(ChannelEntityType.PROGRAMMING);

        return entity;
    }

    public static PostEntity generatePostEntity() {
        PostEntity postEntity = new PostEntity();
        postEntity.setContent("Content" + postEntity.getContent() + Math.random() * 100);
        postEntity.setTitle("Title" + postEntity.getTitle() + Math.random() * 100);
        ChannelEntity entity = new ChannelEntity();
        postEntity.setChannel(entity);

        return postEntity;
    }

    public static List<ChannelEntity> generateChannels() {
        ChannelEntity channel1 = new ChannelEntity();
        channel1.setName("post1-");
        channel1.setType(ChannelEntityType.TOURISM);

        ChannelEntity channel2 = new ChannelEntity();
        channel2.setName("post2-");
        channel2.setType(ChannelEntityType.EDUCATION);

        ChannelEntity channel3 = new ChannelEntity();
        channel3.setName("post3-");
        channel3.setType(ChannelEntityType.PROGRAMMING);

        return Arrays.asList(channel1, channel2, channel3); // Вернем список каналов
    }

    public static List<PostEntity> generatePosts() {
        PostEntity post1 = new PostEntity();
        post1.setContent("post1-content");
        post1.setTitle("post1-title");

        PostEntity post2 = new PostEntity();
        post2.setContent("post2-content");
        post2.setTitle("post2-title");

        PostEntity post3 = new PostEntity();
        post3.setContent("post1-content");
        post3.setTitle("post2-title");

        return Arrays.asList(post1, post2, post3); // Вернем список постов
    }
}
