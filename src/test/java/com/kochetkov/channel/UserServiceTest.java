package com.kochetkov.channel;

import com.kochetkov.channel.dto.user.UserFullDto;
import com.kochetkov.channel.entity.UserEntity;
import com.kochetkov.channel.repository.UserRepository;
import com.kochetkov.channel.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserRepository repository;

    private UserService service;

    @BeforeEach
    private void setUp() {
        repository.deleteAll();
    }

    @Test
    void testFindById_happyPath() {
        // given
        UserEntity userEntity = new UserEntity();
        userEntity.setLogin("Bob");
        userEntity.setPassword("12345678");
        userEntity.setEmail("bob@gmail.com");
        userEntity.setImageUrl("http:/pic.jpg");

        UserEntity saved = repository.save(userEntity);

        // when
        UserFullDto foundDto = service.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getEmail(), foundDto.getEmail());
    }
}
