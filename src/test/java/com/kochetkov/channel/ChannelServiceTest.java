package com.kochetkov.channel;

import com.kochetkov.channel.dto.channel.ChannelFullDto;
import com.kochetkov.channel.dto.post.PostFullDto;
import com.kochetkov.channel.entity.ChannelEntity;
import com.kochetkov.channel.entity.ChannelEntityType;
import com.kochetkov.channel.entity.PostEntity;
import com.kochetkov.channel.repository.ChannelRepository;
import com.kochetkov.channel.service.ChannelService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ChannelServiceTest {

    @Autowired
    private ChannelRepository channelRepository;

    @Autowired
    private ChannelService channelService;

    @BeforeEach
    private void setUp() {
        channelRepository.deleteAll();
    }

    @Test
    void testFindById_happyPath() {
        // given
        ChannelEntity channelEntity = new ChannelEntity();
        channelEntity.setName("Mix fight");
        channelEntity.setType(ChannelEntityType.TOURISM);
        List<PostEntity> postEntity = new ArrayList<>();
        channelEntity.setPosts(postEntity);

        ChannelEntity saved = channelRepository.save(channelEntity);

        // when
        ChannelFullDto channelFullDto = channelService.findById(saved.getId());

        // then
        Assertions.assertNotNull(channelFullDto);
        Assertions.assertEquals(saved.getId(), channelFullDto.getId());
        Assertions.assertEquals(saved.getName(), channelFullDto.getName());
        Assertions.assertEquals(saved.getPosts(), channelFullDto.getPosts());
        Assertions.assertEquals(saved.getType(), channelFullDto.getType());
    }
}
