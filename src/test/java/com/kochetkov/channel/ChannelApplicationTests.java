package com.kochetkov.channel;

import com.kochetkov.channel.entity.UserEntity;
import com.kochetkov.channel.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ChannelApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    void testCreate_happyPath() {
        // given
        UserEntity toSave = generateUser();

        // when
        UserEntity saved = userRepository.save(toSave);

        // then
        Assertions.assertNotNull(saved);
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testFindByLogin_happyPath() {
        // given
        UserEntity toSave = generateUser();
        UserEntity saved = userRepository.save(toSave);
        String loginToSearch = saved.getLogin();

        // when
        UserEntity found = userRepository.findByLogin(loginToSearch);

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
        Assertions.assertEquals(saved.getLogin(), found.getLogin());
    }

    @Test
    void testFindById_happyPath() {
        // given
        UserEntity toSave = generateUser();
        UserEntity saved = userRepository.save(toSave);
        Integer idToSearch = saved.getId();

        // when
        UserEntity found = userRepository.findOneById(idToSearch);

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
        Assertions.assertEquals(saved.getLogin(), found.getLogin());
    }

    //FAKER
    private UserEntity generateUser() {
        UserEntity user = new UserEntity();
        user.setLogin("Login#" + Math.random() * 100);
        user.setPassword("Password#" + Math.random() * 100);
        user.setImageUrl("Image#" + Math.random() * 100);

        return user;
    }
}
