package com.kochetkov.channel;

import com.kochetkov.channel.entity.ChannelEntity;
import com.kochetkov.channel.entity.PostEntity;
import com.kochetkov.channel.repository.ChannelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import  static com.kochetkov.channel.EntityGenerationUtils.*;
import java.util.List;

@SpringBootTest
public class ChannelRepositoryTest {

    @Autowired
    private ChannelRepository repository;

    @Test
    void findById_happyPath() {
        // given
        ChannelEntity toSave = generateChannelEntity();
        List<PostEntity> posts = generatePosts(); // Получаем список рандомных постов

        toSave.setPosts(posts); // Связываем entity между собой!
        for (PostEntity p : posts) {
            p.setChannel(toSave); // А теперь каждый транзитный ENTITY знает про свой канал
        }

        ChannelEntity saved = repository.save(toSave);
        Integer idToSearch = saved.getId(); // берем id ранее сохраненного элемента, чтобы потом его найти в бд

        // when
        ChannelEntity found = repository.findOneById(idToSearch);

        // then
        Assertions.assertNotNull(found);
        Assertions.assertEquals(saved.getId(), found.getId());
        Assertions.assertEquals(3, found.getPosts().size());
    }

    @Test
    void deleteById_happyPath() {
        // given
        ChannelEntity toSave = generateChannelEntity();
        List<PostEntity> posts = generatePosts(); // Получаем список рандомных постов

        toSave.setPosts(posts); // Связываем entity между собой!
        for (PostEntity p : posts) {
            p.setChannel(toSave); // А теперь каждый транзитный ENTITY знает про свой канал
        }

        ChannelEntity saved = repository.save(toSave);
        Integer idToSearch = saved.getId(); // берем id ранее сохраненного элемента, чтобы потом его найти в бд

        // when
        repository.deleteById(idToSearch); // УДАЛАЯЕМ КАНАЛ
        ChannelEntity found = repository.findOneById(idToSearch);

        // then
        Assertions.assertNull(found);
    }
}
