package com.kochetkov.channel;

import com.kochetkov.channel.dto.post.PostFullDto;
import com.kochetkov.channel.entity.ChannelEntity;
import com.kochetkov.channel.entity.PostEntity;
import com.kochetkov.channel.repository.PostRepository;
import com.kochetkov.channel.service.PostService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PostServiceTest {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private PostService postService;

    @BeforeEach
    private void setUp() {
        postRepository.deleteAll();
    }

    @Test
    void testFindById_happyPath() {
        // given
        PostEntity postEntity = new PostEntity();
        postEntity.setContent("Youtoob");
        postEntity.setTitle("Entermedia");
        ChannelEntity channelEntity = new ChannelEntity();
        postEntity.setChannel(channelEntity);

        PostEntity saved = postRepository.save(postEntity);

        // when
        PostFullDto foundDto = postService.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundDto);
        Assertions.assertEquals(saved.getId(), foundDto.getId());
        Assertions.assertEquals(saved.getChannel(), foundDto.getChannelId());
        Assertions.assertEquals(saved.getContent(), foundDto.getContent());
        Assertions.assertEquals(saved.getTitle(), foundDto.getTitle());
    }
}
